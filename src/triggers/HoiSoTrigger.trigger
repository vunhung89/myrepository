/**
* クラス名  	:	HoiSoTrigger
* クラス概要 	:	HoiSoTrigger
* @created  :	2015/04/20 ksvc Nhung Vu
* @modified :	
*/
trigger HoiSoTrigger on HoiSo__c (after update) {
	
	// HoiSoTriggerハンドラ
	HoiSoTriggerHandler handler = new HoiSoTriggerHandler();
	
	// ・「after update」の場合、
	if(trigger.isAfter && trigger.isUpdate){
		// Control this trigger cannnot excecute update ChiNhanh__c.TongTienVNDCN many times
		if(!CommonValues.isHoiSoTrigger){
			system.debug('----- Call trigger HoiSo -----');
			CommonValues.isHoiSoTrigger = true;
			handler.onAfterUpdate(trigger.newMap);
			CommonValues.isHoiSoTrigger = false;
		}
	}
}