/**
* クラス名  	:	HoiSoTriggerDAO
* クラス概要 	:	HoiSoTriggerDAO
* @created  :	2015/04/20 ksvc Nhung Vu
* @modified :	
*/
public with sharing class HoiSoTriggerDAO {
	
	/**
	* getListChiNhanhByHoiSoIds
	* Get list ChiNhanh__c based on list Id of HoiSo__c
	* @param mapHoiSo(trigger.new) HoiSoリスト
	* @return　なし
	* @created: 2015/04/20 ksvc Nhung Vu
	* @modified: 
	*/
	public static list<ChiNhanh__c> getListChiNhanhByHoiSoIds(Set<Id> setHoiSoId){
		
		list<ChiNhanh__c> listChiNhanh = new list<ChiNhanh__c>();
		
		listChiNhanh = [Select Id, Name, VND__c, USD__c, AUD__c, TongTienVNDCN__c, HoiSo__c
						From ChiNhanh__c 
						Where HoiSo__r.Id IN : setHoiSoId order by Name, TongTienVNDCN__c];
		
		return listChiNhanh;
	}
}